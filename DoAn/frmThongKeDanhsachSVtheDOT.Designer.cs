﻿
namespace DoAn
{
    partial class frmThongKeDanhsachSVtheDOT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboSVtheoDotTT = new System.Windows.Forms.ComboBox();
            this.btnTK = new System.Windows.Forms.Button();
            this.btnBC = new System.Windows.Forms.Button();
            this.dgvSVtheoDoyTT = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSVtheoDoyTT)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(280, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(639, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "THỐNG KÊ SINH VIÊN THỰC TẬP THEO ĐỢT";
            // 
            // cboSVtheoDotTT
            // 
            this.cboSVtheoDotTT.FormattingEnabled = true;
            this.cboSVtheoDotTT.Location = new System.Drawing.Point(286, 197);
            this.cboSVtheoDotTT.Name = "cboSVtheoDotTT";
            this.cboSVtheoDotTT.Size = new System.Drawing.Size(349, 24);
            this.cboSVtheoDotTT.TabIndex = 1;
            // 
            // btnTK
            // 
            this.btnTK.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTK.Location = new System.Drawing.Point(680, 197);
            this.btnTK.Name = "btnTK";
            this.btnTK.Size = new System.Drawing.Size(135, 34);
            this.btnTK.TabIndex = 2;
            this.btnTK.Text = "THỐNG KÊ";
            this.btnTK.UseVisualStyleBackColor = false;
            this.btnTK.Click += new System.EventHandler(this.btnTK_Click);
            // 
            // btnBC
            // 
            this.btnBC.BackColor = System.Drawing.Color.Red;
            this.btnBC.Location = new System.Drawing.Point(849, 193);
            this.btnBC.Name = "btnBC";
            this.btnBC.Size = new System.Drawing.Size(123, 34);
            this.btnBC.TabIndex = 3;
            this.btnBC.Text = "BÁO CÁO";
            this.btnBC.UseVisualStyleBackColor = false;
            this.btnBC.Click += new System.EventHandler(this.btnBC_Click);
            // 
            // dgvSVtheoDoyTT
            // 
            this.dgvSVtheoDoyTT.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvSVtheoDoyTT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSVtheoDoyTT.Location = new System.Drawing.Point(12, 320);
            this.dgvSVtheoDoyTT.Name = "dgvSVtheoDoyTT";
            this.dgvSVtheoDoyTT.RowHeadersWidth = 51;
            this.dgvSVtheoDoyTT.RowTemplate.Height = 24;
            this.dgvSVtheoDoyTT.Size = new System.Drawing.Size(1149, 366);
            this.dgvSVtheoDoyTT.TabIndex = 4;
            // 
            // frmThongKeDanhsachSVtheDOT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1218, 728);
            this.Controls.Add(this.dgvSVtheoDoyTT);
            this.Controls.Add(this.btnBC);
            this.Controls.Add(this.btnTK);
            this.Controls.Add(this.cboSVtheoDotTT);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmThongKeDanhsachSVtheDOT";
            this.Text = "frmDanhsachSVtheDOT";
            this.Load += new System.EventHandler(this.frmThongKeDanhsachSVtheDOT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSVtheoDoyTT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSVtheoDotTT;
        private System.Windows.Forms.Button btnTK;
        private System.Windows.Forms.Button btnBC;
        private System.Windows.Forms.DataGridView dgvSVtheoDoyTT;
    }
}