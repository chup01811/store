﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoAn
{
    public partial class frmHome : Form
    {
        public frmHome()
        {
            InitializeComponent();
        }

        private void quảnLýSinhViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSinhvien sv = new frmSinhvien();
            sv.Show();
        }

        private void quảnLýĐợtThựcTậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDotTT dot = new frmDotTT();
            dot.Show();
        }

        private void quảnLýKếtQuảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSinhvienHD svhd = new frmSinhvienHD();
            svhd.Show();
        }

        private void quảnLýHDDGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHDDG hd = new frmHDDG();
            hd.Show();
        }

        private void quảnLýĐịaĐiểmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDiadiem dd = new frmDiadiem();
            dd.Show();
        }

        private void quảnLýGVHDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGVHD gv = new frmGVHD();
                gv.Show();
        }

        private void quảnLýĐềTàiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDetai dt = new frmDetai();
            dt.Show();
        }

        private void thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmThongKeDanhsachSVtheDOT sctd = new frmThongKeDanhsachSVtheDOT();
            sctd.Show();
        }

        private void thốngKêSinhViênTheoHDDGToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmThongkeSinhvientheoHDDG svthd = new frmThongkeSinhvientheoHDDG();
            svthd.Show();
        }

        private void thốngKêĐiểmCủaSinhViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmThongKeDiem d = new frmThongKeDiem();
            d.Show();
        }

        private void báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBaoCaoDiadiemTTtheoDot dtd = new frmBaoCaoDiadiemTTtheoDot();
            dtd.Show();
        }

        private void quảnLýSinhViênThựcTậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSinhvienTT _svtt = new frmSinhvienTT();
            _svtt.Show();
        }

        private void thànhViênHộiĐồngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmThanhvienHD _tt = new frmThanhvienHD();
            _tt.Show();
        }
    }
}
