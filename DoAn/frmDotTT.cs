﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmDotTT : Form
    {
        DotTTEntities _dotent = new DotTTEntities();
        DotTTBLL _dotbll = new DotTTBLL();
        public frmDotTT()
        {
            InitializeComponent();
        }

        private void frmDotTT_Load(object sender, EventArgs e)
        {
            _dotbll.SelectAllDotTT(dgvDotTT);
        }

        private void dgvDotTT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvDotTT.CurrentRow.Cells[0].Value.ToString();
            txtND.Text = dgvDotTT.CurrentRow.Cells[1].Value.ToString();
            dtpNgayBD.Value = DateTime.Parse(dgvDotTT.CurrentRow.Cells[2].Value.ToString());
            dtpNgayKT.Value = DateTime.Parse(dgvDotTT.CurrentRow.Cells[3].Value.ToString());
            txtGhichu.Text = dgvDotTT.CurrentRow.Cells[4].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            {
                MessageBox.Show("Mã Dợt TT không được để trống");
            }
            else if (txtND.Text == "")
            {
                MessageBox.Show("ND Dợt TT không được để trống");
            }
            else
            {
                try
                {
                    _dotent.MaDotTT = txtMa.Text;
                    _dotent.ND = txtND.Text;
                    _dotent.NgayBD = dtpNgayBD.Value;
                    _dotent.NgayKT_Dukien = dtpNgayKT.Value;
                    _dotent.Ghichu = txtGhichu.Text;
                    _dotbll.InsertDotTT(_dotent);
                    frmDotTT_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Dợt thực tập không được trùng lặp vui lòng điền đầy đủ thông tin");
                }

            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _dotent.MaDotTT = txtMa.Text;
            _dotent.ND = txtND.Text;
            _dotent.NgayBD = dtpNgayBD.Value;
            _dotent.NgayKT_Dukien = dtpNgayKT.Value;
            _dotent.Ghichu = txtGhichu.Text;

            _dotbll.UpdateDotTT(_dotent);
            frmDotTT_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _dotent.MaDotTT = txtMa.Text;
            _dotbll.DeleteDotTT(_dotent);
            frmDotTT_Load(sender, e);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtND.Clear();
            txtGhichu.Clear();
            _dotbll.SelectAllDotTT(dgvDotTT);
        }
    }
}
