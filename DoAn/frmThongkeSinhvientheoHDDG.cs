﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmThongkeSinhvientheoHDDG : Form
    {
        SinhvienBLL _svbll = new SinhvienBLL();
        HDDGBLL _hdbll = new HDDGBLL();
        public frmThongkeSinhvientheoHDDG()
        {
            InitializeComponent();
        }

        private void cboSVtheHD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmThongkeSinhvientheoHDDG_Load(object sender, EventArgs e)
        {
            _hdbll.SelectAll(cboSVtheHD);
        }

        private void btnTK_Click(object sender, EventArgs e)
        {
            _hdbll.SelectSVtheoDot(dgvSVtheoHD, cboSVtheHD.SelectedValue.ToString());
        }

        private void btnBC_Click(object sender, EventArgs e)
        {
            crytalSVtheoHD _crpsvtheohd = new crytalSVtheoHD();
            DataSet ds = new DataSet();
            ds = _hdbll.SelectSVtheoDot(cboSVtheHD.SelectedValue.ToString());
            _crpsvtheohd.SetDataSource(ds.Tables[0]);

            frmCrytalSVtheoHD _frmsvtheodd = new frmCrytalSVtheoHD();
            _frmsvtheodd.crystalReportViewer1.ReportSource = _crpsvtheohd;
            _frmsvtheodd.Show();
        }
    }
}
