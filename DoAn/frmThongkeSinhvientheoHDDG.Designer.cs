﻿
namespace DoAn
{
    partial class frmThongkeSinhvientheoHDDG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cachedcrytalSVtheoDotTT1 = new DoAn.CachedcrytalSVtheoDotTT();
            this.cboSVtheHD = new System.Windows.Forms.ComboBox();
            this.btnTK = new System.Windows.Forms.Button();
            this.btnBC = new System.Windows.Forms.Button();
            this.dgvSVtheoHD = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSVtheoHD)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(275, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(630, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "THỐNG KÊ SINH VIÊN HỘI ĐỒNG ĐÁNH GIÁ";
            // 
            // cboSVtheHD
            // 
            this.cboSVtheHD.FormattingEnabled = true;
            this.cboSVtheHD.Location = new System.Drawing.Point(281, 156);
            this.cboSVtheHD.Name = "cboSVtheHD";
            this.cboSVtheHD.Size = new System.Drawing.Size(332, 24);
            this.cboSVtheHD.TabIndex = 1;
            this.cboSVtheHD.SelectedIndexChanged += new System.EventHandler(this.cboSVtheHD_SelectedIndexChanged);
            // 
            // btnTK
            // 
            this.btnTK.BackColor = System.Drawing.Color.SeaShell;
            this.btnTK.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTK.Location = new System.Drawing.Point(636, 156);
            this.btnTK.Name = "btnTK";
            this.btnTK.Size = new System.Drawing.Size(105, 33);
            this.btnTK.TabIndex = 2;
            this.btnTK.Text = "THỐNG KÊ";
            this.btnTK.UseVisualStyleBackColor = false;
            this.btnTK.Click += new System.EventHandler(this.btnTK_Click);
            // 
            // btnBC
            // 
            this.btnBC.BackColor = System.Drawing.Color.Red;
            this.btnBC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBC.ForeColor = System.Drawing.Color.Black;
            this.btnBC.Location = new System.Drawing.Point(769, 156);
            this.btnBC.Name = "btnBC";
            this.btnBC.Size = new System.Drawing.Size(101, 32);
            this.btnBC.TabIndex = 3;
            this.btnBC.Text = "BÁO CÁO";
            this.btnBC.UseVisualStyleBackColor = false;
            this.btnBC.Click += new System.EventHandler(this.btnBC_Click);
            // 
            // dgvSVtheoHD
            // 
            this.dgvSVtheoHD.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvSVtheoHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSVtheoHD.Location = new System.Drawing.Point(22, 260);
            this.dgvSVtheoHD.Name = "dgvSVtheoHD";
            this.dgvSVtheoHD.RowHeadersWidth = 51;
            this.dgvSVtheoHD.RowTemplate.Height = 24;
            this.dgvSVtheoHD.Size = new System.Drawing.Size(1076, 387);
            this.dgvSVtheoHD.TabIndex = 4;
            // 
            // frmThongkeSinhvientheoHDDG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.ClientSize = new System.Drawing.Size(1134, 659);
            this.Controls.Add(this.dgvSVtheoHD);
            this.Controls.Add(this.btnBC);
            this.Controls.Add(this.btnTK);
            this.Controls.Add(this.cboSVtheHD);
            this.Controls.Add(this.label1);
            this.Name = "frmThongkeSinhvientheoHDDG";
            this.Text = "frmThongkeSinhvientheoHDDG";
            this.Load += new System.EventHandler(this.frmThongkeSinhvientheoHDDG_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSVtheoHD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CachedcrytalSVtheoDotTT cachedcrytalSVtheoDotTT1;
        private System.Windows.Forms.ComboBox cboSVtheHD;
        private System.Windows.Forms.Button btnTK;
        private System.Windows.Forms.Button btnBC;
        private System.Windows.Forms.DataGridView dgvSVtheoHD;
    }
}