﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmSinhvienTT : Form
    {
        SinhvienBLL _svbll = new SinhvienBLL();
        DotTTBLL _dotbll = new DotTTBLL();
        DiadiemBLL _dd = new DiadiemBLL();
        DetaiBLL _dt = new DetaiBLL();
        SinhvienTTBLL _svtt = new SinhvienTTBLL();
        SinhvienTTEntines _svent = new SinhvienTTEntines();
        public frmSinhvienTT()
        {
            InitializeComponent();
        }

        private void frmSinhvienTT_Load(object sender, EventArgs e)
        {
            _svtt.SelectAllSinhvienTT(dgvSVtt);
            _svbll.SelectAll(listBox1);
            _dotbll.SelectAll(cboMadot);

            _dd.SelectAll(cboMadd);
            _dt.SelectAllMa(cboMadt);


        }

        private void dgvSVtt_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //txtMa.Text = dgvSVtt.CurrentRow.Cells[0].Value.ToString();
            cboMadot.SelectedValue = dgvSVtt.CurrentRow.Cells[0].Value.ToString();
            cboMasv.SelectedValue = dgvSVtt.CurrentRow.Cells[1].Value.ToString();
            cboMadd.SelectedValue = dgvSVtt.CurrentRow.Cells[2].Value.ToString();
            cboMadt.SelectedValue = dgvSVtt.CurrentRow.Cells[3].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            foreach (object pt in listBox1.SelectedItems)
            {
                //_svent.MaSVTT = txtMa.Text;
                _svent.MaDotTT = cboMadot.SelectedValue.ToString();
                _svent.MaSV = listBox1.SelectedValue.ToString();
                _svent.MaDiadiem = cboMadd.SelectedValue.ToString();
                _svent.MaDetai = cboMadt.SelectedValue.ToString();
                _svtt.InsertSinhvienTT(_svent);
            }




            frmSinhvienTT_Load(sender, e);




            
                
            
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            //_svent.MaSVTT = txtMa.Text;
            _svent.MaDotTT = cboMadot.SelectedValue.ToString();
            _svent.MaSV = cboMasv.SelectedValue.ToString();
            _svent.MaDiadiem = cboMadd.SelectedValue.ToString();
            _svent.MaDetai = cboMadt.SelectedValue.ToString();
            _svtt.UpdateStudentTT(_svent);
            frmSinhvienTT_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try { 
            //_svent.MaSVTT = txtMa.Text;
            _svtt.DeleteStudentTT(_svent);
            frmSinhvienTT_Load(sender, e);
            }
            catch
            {
                MessageBox.Show("Sinh viên thực tập đã được sắp xếp vui lòng cập nhập lại các bảng liên quan nếu bạn muốn xóa sữ liệu ");
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            //txtMa.Clear();
            frmSinhvienTT_Load(sender, e);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }

        private void lstbMaSV_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void sl1_Click(object sender, EventArgs e)

        {
            //lstb2.Items.Add(lstbMaSV.SelectedItem.ToString());
            //_svbll.SelectAll(lstb2.Items.Add(lstbMaSV.SelectedItem.ToString()));
            //lstbMaSV.Sele.Remove(lstbMaSV.SelectedItem);
            //DataRowView drv = (DataRowView)lstb2.SelectedItem;
            //String  = drv["MaSV"].ToString();
            //_svbll.SelectAll


        }
    }
    }

