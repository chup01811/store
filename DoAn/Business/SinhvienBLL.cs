﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class SinhvienBLL
    {
        SinhvienDAO _svda = new SinhvienDAO();
        public void SelectAllSinhvien(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _svda.SinhvienSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }

        public DataSet SelectStudentByID(string ID, DataGridView dgv)
        {
            SqlParameter pr = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr.Value = ID;
            DataSet ds = new DataSet();
            ds = _svda.SinhvienSelectByID(pr);
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }

        public DataSet SelectStudentByName(string ID, DataGridView dgv)
        {
            SqlParameter pr = new SqlParameter("@TenSV", SqlDbType.NVarChar, 50);
            pr.Value = ID;
            DataSet ds = new DataSet();
            ds = _svda.SinhvienSelectByName(pr);
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }
        public void SelectAll(ListBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _svda.SinhvienSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaSV";
            cbo.ValueMember = "MaSV";
            
        }
    

        public void SelectAllMa(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _svda.SinhvienSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaSV";
            cbo.ValueMember = "MaSV";
        }
        //public DataSet SelectStudentByYear(DataGridView dgv, int _tunam, int _dennam)
        //{
        //SqlParameter[] pr = new SqlParameter[2];
        //SqlParameter pr0 = new SqlParameter("@Tunam", SqlDbType.Int);
        // pr0.Value = _tunam;
        // pr[0] = pr0;
        //SqlParameter pr1 = new SqlParameter("@Dennam", SqlDbType.Int);
        //pr1.Value = _dennam;
        // pr[1] = pr1;
        //DataSet ds = new DataSet();
        // ds = _objSVDAO.SelectStudentByWhere("Sinhvien_SelectByYear ", pr);

        //dgv.DataSource = ds;
        //dgv.DataMember = ds.Tables[0].TableName.ToString();
        // return ds;
        //}

        // public void SelectAllMaSV(ComboBox cbo)
        //{
        //   DataSet ds = new DataSet();
        //  ds = _objSVDAO.SinhvienSelectAll();
        //  cbo.DataSource = ds.Tables[0];
        //  cbo.DisplayMember = "MaSV";
        //  cbo.ValueMember = "MaSV";
        // }
        // public void SelectAllTenSV(ComboBox cbo)
        //{
        //// DataSet ds = new DataSet();
        //ds = _objSVDAO.SinhvienSelectAll();
        //cbo.DataSource = ds.Tables[0];
        // cbo.DisplayMember = "TenSV";
        //cbo.ValueMember = "TenSV";
        // }
        public SqlParameter[] Sinhvien_Parameter(SinhvienEntities sv)
        {
            SqlParameter[] pr = new SqlParameter[4];
            SqlParameter pr0 = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr0.Value = sv.MaSV;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@TenSV", SqlDbType.NVarChar, 50);
            pr1.Value = sv.TenSV;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@Ngaysinh", SqlDbType.Date);
            pr2.Value = sv.Ngaysinh;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@Gioitinh", SqlDbType.Bit);
            pr3.Value = sv.Gioitinh;
            pr[3] = pr3;
            return pr;
        }
        public void InsertSinhvien(SinhvienEntities sv)
        {
            _svda.InsertSinhVien(Sinhvien_Parameter(sv));
        }
        public void UpdateStudent(SinhvienEntities sv)
        {
            _svda.UpdateSinhVien(Sinhvien_Parameter(sv));
        }
        public void DeleteStudent(SinhvienEntities sv)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr0.Value = sv.MaSV;
            pr[0] = pr0;
            _svda.DeleteSinhVien(pr);
        }

    }
}
