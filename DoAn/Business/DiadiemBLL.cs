﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class DiadiemBLL
    {
        DiadiemDAO _ddda = new DiadiemDAO();
        public void SelectAllDiadiem(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _ddda.DiadiemSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
        public void SelectAll(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _ddda.DiadiemSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "TenDiadiem";
            cbo.ValueMember = "MaDiadiem";
        }
       // public void SelectAllMa(ComboBox cbo)
       // {
          //  DataSet ds = new DataSet();
           // ds = _ddda.DiadiemSelectAll();
           // cbo.DataSource = ds.Tables[0];
            //cbo.DisplayMember = "MaDDTT";
            //cbo.ValueMember = "MaDDTT";
       // }

        public SqlParameter[] Diadiem_Parameter(DiadiemEntites dd)
        {
            SqlParameter[] pr = new SqlParameter[4];
            SqlParameter pr0 = new SqlParameter("@MaDiadiem", SqlDbType.NVarChar, 10);
            pr0.Value = dd.MaDiadiem;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@TenDiadiem", SqlDbType.NVarChar, 50);
            pr1.Value = dd.TenDiadiem;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@Diachi", SqlDbType.NVarChar, 50);
            pr2.Value = dd.Diachi;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@SDT", SqlDbType.NVarChar, 50);
            pr3.Value = dd.SDT;
            pr[3] = pr3;

            return pr;
        }
        public void InsertDiadiem(DiadiemEntites dd)
        {
            _ddda.InsertDiadiem(Diadiem_Parameter(dd));
        }
        public void UpdateDiadiem(DiadiemEntites dd)
        {
            _ddda.UpdateDiadiem(Diadiem_Parameter(dd));
        }
        public void DeleteDiadiem(DiadiemEntites dd)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDiadiem", SqlDbType.NVarChar, 50);
            pr0.Value = dd.MaDiadiem;
            pr[0] = pr0;
            _ddda.DeleteDiadiem(pr);
        }
    }
}
