﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class DetaiBLL
    {
        DetaiDAO _ddda = new DetaiDAO();
        public void SelectAllDetai(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _ddda.DetaiSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
        public void SelectAllMa(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _ddda.DetaiSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaDetai";
            cbo.ValueMember = "MaDetai";
        }

        public SqlParameter[] Detai_Parameter(DetaiEntities dd)
        {
            SqlParameter[] pr = new SqlParameter[4];
            SqlParameter pr0 = new SqlParameter("@MaDetai", SqlDbType.NVarChar, 50);
            pr0.Value = dd.MaDetai;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@TenDetai", SqlDbType.NVarChar, 150);
            pr1.Value = dd.TenDetai;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@MaGV", SqlDbType.NVarChar, 50);
            pr2.Value = dd.MaGV;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@Ghichu", SqlDbType.NVarChar, 50);
            pr3.Value = dd.Ghichu;
            pr[3] = pr3;

            return pr;
        }
        public void InsertDietai(DetaiEntities dd)
        {
            _ddda.InsertDetai(Detai_Parameter(dd));
        }
        public void UpdateDetai(DetaiEntities dd)
        {
            _ddda.UpdateDetai(Detai_Parameter(dd));
        }
        public void DeleteDetai(DetaiEntities dd)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDetai", SqlDbType.NVarChar, 50);
            pr0.Value = dd.MaDetai;
            pr[0] = pr0;
            _ddda.DeleteDetai(pr);
        }
    }
}
