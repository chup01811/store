﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class SinhvienTTBLL
    {
        SinhvienTTDAO _svda = new SinhvienTTDAO();
        public void SelectAllSinhvienTT(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _svda.SinhvienTTSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }

    
        public SqlParameter[] SinhvienTT_Parameter(SinhvienTTEntines sv)
        {
            SqlParameter[] pr = new SqlParameter[4];
          
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = sv.MaDotTT;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr1.Value = sv.MaSV;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@MaDiadiem", SqlDbType.NVarChar, 50);
            pr2.Value = sv.MaDiadiem;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@MaDetai", SqlDbType.NVarChar, 50);
            pr3.Value = sv.MaDetai;
             pr[3] = pr3;
              return pr;
            }
        public void InsertSinhvienTT(SinhvienTTEntines sv)
        {
            _svda.InsertSinhvienTT(SinhvienTT_Parameter(sv));
        }
        public void UpdateStudentTT(SinhvienTTEntines sv)
        {
            _svda.UpdateSinhvienTT(SinhvienTT_Parameter(sv));
        }
        public void DeleteStudentTT(SinhvienTTEntines sv)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr0.Value = sv.MaSV;
            pr[0] = pr0;
            _svda.DeleteSinhvienTT(pr);
        }
    }
}
