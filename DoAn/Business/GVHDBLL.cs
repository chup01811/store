﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class GVHDBLL
    {
        GVHĐAO _gvda = new GVHĐAO();
        public void SelectAllGVHD(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _gvda.GVHDSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
        public void SelectAll(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _gvda.GVHDSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaGV";
            cbo.ValueMember = "MaGV";
        }

        public SqlParameter[] GVHD_Parameter(GVHDEntites gv)
        {
            SqlParameter[] pr = new SqlParameter[5];
            SqlParameter pr0 = new SqlParameter("@MaGV", SqlDbType.NVarChar, 50);
            pr0.Value = gv.MaGV;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@TenGV", SqlDbType.NVarChar, 50);
            pr1.Value = gv.TenGV;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@Gioitinh", SqlDbType.Bit);
            pr2.Value = gv.Gioitinh;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@Email", SqlDbType.NVarChar, 50);
            pr3.Value = gv.Email;
            pr[3] = pr3;
            SqlParameter pr4 = new SqlParameter("@SDT", SqlDbType.NVarChar, 50);
            pr4.Value = gv.SDT;
            pr[4] = pr4;

            return pr;
        }
        public void InsertGVHD(GVHDEntites gv)
        {
            _gvda.InsertGVHD(GVHD_Parameter(gv));
        }
        public void UpdateGVHD(GVHDEntites gv)
        {
            _gvda.UpdateGVHD(GVHD_Parameter(gv));
        }
        public void DeleteGVHD(GVHDEntites gv)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaGV", SqlDbType.NVarChar, 50);
            pr0.Value = gv.MaGV;
            pr[0] = pr0;
            _gvda.DeleteGVHD(pr);
        }
    }
}
