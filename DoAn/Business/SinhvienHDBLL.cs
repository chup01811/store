﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class SinhvienHDBLL
    {
        SinhvienHDDAO _svda = new SinhvienHDDAO();
        public void SelectAllSinhvien(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _svda.SinhvienHDSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }

        public DataSet SelectDiemcuaSV(DataGridView dgv, float _tudiem, float _dendiem)
        {
            SqlParameter[] pr = new SqlParameter[2];
            SqlParameter pr0 = new SqlParameter("@Tudiem", SqlDbType.Float);
            pr0.Value = _tudiem;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@Dendiem", SqlDbType.Float);
            pr1.Value = _dendiem;
            pr[1] = pr1;
            DataSet ds = new DataSet();
            ds = _svda.SelectDiemcuaSVe("tblThongkiemDiem", pr);

            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }


        public SqlParameter[] SinhvienHD_Parameter(SinhvienHDEntines sv)
        {
            SqlParameter[] pr = new SqlParameter[5];
            SqlParameter pr0 = new SqlParameter("@Ma", SqlDbType.NVarChar, 50);
            pr0.Value = sv.Ma;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr1.Value = sv.MaHDDG;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@MaSV", SqlDbType.NVarChar, 50);
            pr2.Value = sv.MaSV;
            pr[2] = pr2;
            //SqlParameter pr3 = new SqlParameter("@Ghichu", SqlDbType.DateTime);
            //pr3.Value = sv.Ghichu;
            //pr[3] = pr3;
            SqlParameter pr3 = new SqlParameter("@Diem", SqlDbType.Float);
            pr3.Value = sv.Diem;
            pr[3] = pr3;
            SqlParameter pr4 = new SqlParameter("@Thoigianbaove", SqlDbType.NVarChar, 50);
            pr4.Value = sv.Thoigianbaove;
            pr[4] = pr4;
            return pr;
        }
        public void InsertSinhvienHD(SinhvienHDEntines sv)
        {
            _svda.InsertSinhvienHD(SinhvienHD_Parameter(sv));
        }
        public void UpdateStudentHD(SinhvienHDEntines sv)
        {
            _svda.UpdateSinhvienHD(SinhvienHD_Parameter(sv));
        }
        public void DeleteStudentHD(SinhvienHDEntines sv)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@Ma", SqlDbType.NVarChar, 50);
            pr0.Value = sv.Ma;
            pr[0] = pr0;
            _svda.DeleteSinhvienHD(pr);
        }
    }
}
