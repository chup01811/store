﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
   
    class HDDGBLL
    {
        HDDGDAO _hdda = new HDDGDAO();
        public void SelectAllHDDG(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _hdda.HDDGSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
        //public void SelectAll(ComboBox cbo)
        //{
            //DataSet ds = new DataSet();
           // ds = _hdda.HDDGSelectAll();
            //cbo.DataSource = ds.Tables[0];
            //cbo.DisplayMember = "Thanhvien";
            //cbo.ValueMember = "MaHDDGTT";
        //}
       
        public void SelectAll(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _hdda.HDDGSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaHDDG";
            cbo.ValueMember = "MaHDDG";
        }
        public DataSet SelectSVtheoDot(DataGridView dgv, string _MaHD)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr0.Value = _MaHD;
            pr[0] = pr0;
            DataSet ds = new DataSet();
            ds = _hdda.SelectSVtheoHDbBywhere("tblSinhvienSelect_HDDG", pr);

            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }
        //public DataSet SelectHDDGtheoDetai(DataGridView dgv, string _maDotTT)
        //{
        // SqlParameter[] pr = new SqlParameter[1];
        //SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 10);
        //pr0.Value = _maDotTT;
        //pr[0] = pr0;
        //DataSet ds = new DataSet();
        //ds = _dotda.SelectSVtheoDOTbBywhere("tblSinhvienSelect_DotTT", pr);
        //dgv.DataSource = ds;
        //dgv.DataMember = ds.Tables[0].TableName.ToString();
        //return ds;
        //}

        public DataSet SelectSVtheoDot(string _MaHD)
        {
            DataSet ds = new DataSet();
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr0.Value = _MaHD;
            pr[0] = pr0;

            ds = _hdda.SelectSVtheoHDbBywhere("tblSinhvienSelect_HDDG", pr);


            return ds;
        }
        public SqlParameter[] HDDG_Parameter(HDDGEntites hd)
        {
            SqlParameter[] pr = new SqlParameter[5];
            SqlParameter pr0 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr0.Value = hd.MaHDDG;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@ND", SqlDbType.NVarChar, 50);
            pr1.Value = hd.ND;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr2.Value = hd.MaDotTT;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@NgayBVdukien", SqlDbType.DateTime);
            pr3.Value = hd.NgayBVdukien;
            pr[3] = pr3;
            SqlParameter pr4 = new SqlParameter("@NgayBVchinhthuc", SqlDbType.DateTime);
            pr4.Value = hd.NgayBVchinhthuc;
            pr[4] = pr4;

            return pr;
        }
        public void InsertHDDG(HDDGEntites hd)
        {
            _hdda.InsertHDDG(HDDG_Parameter(hd));
        }
        public void UpdateHDDG(HDDGEntites hd)
        {
            _hdda.UpdateHDDG(HDDG_Parameter(hd));
        }
        public void DeleteHDDG(HDDGEntites hd)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr0.Value = hd.MaHDDG;
            pr[0] = pr0;
            _hdda.DeleteHDDG(pr);
        }
    }
}
