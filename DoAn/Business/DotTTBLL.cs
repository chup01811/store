﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class DotTTBLL
    {
        DotTTDAO _dotda = new DotTTDAO();
        public void SelectAllDotTT(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _dotda.DotTTSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
        public void SelectAll(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _dotda.DotTTSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaDotTT";
            cbo.ValueMember = "MaDotTT";
        }
        
        
        public void SelectAllMa(ComboBox cbo)
        {
            DataSet ds = new DataSet();
            ds = _dotda.DotTTSelectAll();
            cbo.DataSource = ds.Tables[0];
            cbo.DisplayMember = "MaDotTT";
            cbo.ValueMember = "MaDotTT";
        }
        public DataSet SelectHDtheoDot(DataGridView dgv, string _MaDotTT)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = _MaDotTT;
            pr[0] = pr0;
            DataSet ds = new DataSet();
            ds = _dotda.SelectHDtheoDOTbBywhere("tblHDDGtheodotTT", pr);

            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }

        public DataSet SelectSinhvientheoDot(DataGridView dgv, string _maDotTT)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = _maDotTT;
            pr[0] = pr0;
            DataSet ds = new DataSet();
            ds = _dotda.SelectSVtheoDOTbBywhere("tblSinhvienSelect_DotTT", pr);
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();
            return ds;
        }

        public DataSet SelectSinhvientheoDot(string _maDotTT)
        {
            DataSet ds = new DataSet();
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = _maDotTT;
            pr[0] = pr0;

            ds = _dotda.SelectSVtheoDOTbBywhere("tblSinhvienSelect_DotTT", pr);

            return ds;
        }

        public DataSet SelectDiadiemtheoDot(string _maDotTT)
        {
            DataSet ds = new DataSet();
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = _maDotTT;
            pr[0] = pr0;

            ds = _dotda.SelectSVtheoDOTbBywhere("tblDiadiemtheoDOT", pr);

            return ds;
        }

       



       



        public SqlParameter[] DotTT_Parameter(DotTTEntities dt)
        {
            SqlParameter[] pr = new SqlParameter[5];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = dt.MaDotTT;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@ND", SqlDbType.NVarChar, 50);
            pr1.Value = dt.ND;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@NgayBD", SqlDbType.DateTime);
            pr2.Value = dt.NgayBD;
            pr[2] = pr2;
            SqlParameter pr3 = new SqlParameter("@NgayKT_Dukien", SqlDbType.DateTime);
            pr3.Value = dt.NgayKT_Dukien;
            pr[3] = pr3;
            SqlParameter pr4 = new SqlParameter("@Ghichu", SqlDbType.NVarChar, 50);
            pr4.Value = dt.Ghichu;
            pr[4] = pr4;
            return pr;
        }
        public void InsertDotTT(DotTTEntities dt)
        {
            _dotda.InsertDotTT(DotTT_Parameter(dt));
        }
        public void UpdateDotTT(DotTTEntities dt)
        {
            _dotda.UpdateDotTT(DotTT_Parameter(dt));
        }
        public void DeleteDotTT(DotTTEntities dt)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaDotTT", SqlDbType.NVarChar, 50);
            pr0.Value = dt.MaDotTT;
            pr[0] = pr0;
            _dotda.DeleteDotTT(pr);
        }
    }
}
