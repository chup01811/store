﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DoAn.Entities;
using DoAn.DataLayer;
using System.Windows.Forms;

namespace DoAn.Business
{
    class ThanhvienHDBLL
    {
        ThanhvienHDDAO _gvda = new ThanhvienHDDAO();
        public void SelectAllThanhvienHD(DataGridView dgv)
        {
            DataSet ds = new DataSet();
            ds = _gvda.ThanhvienHDSelectAll();
            dgv.DataSource = ds;
            dgv.DataMember = ds.Tables[0].TableName.ToString();

        }
       
        
        public SqlParameter[] ThanhvienHD_Parameter(ThanhvienHDEntines gv)
        {
            SqlParameter[] pr = new SqlParameter[3];
            SqlParameter pr0 = new SqlParameter("@MaThanhvien", SqlDbType.NVarChar, 50);
            pr0.Value = gv.MaThanhvien;
            pr[0] = pr0;
            SqlParameter pr1 = new SqlParameter("@MaHDDG", SqlDbType.NVarChar, 50);
            pr1.Value = gv.MaHDDG;
            pr[1] = pr1;
            SqlParameter pr2 = new SqlParameter("@MaGV", SqlDbType.NVarChar, 50);
            pr2.Value = gv.MaGV;
            pr[2] = pr2;
         
            return pr;
        }
        public void InsertThanhvienHD(ThanhvienHDEntines gv)
        {
            _gvda.InsertThanhvienHD(ThanhvienHD_Parameter(gv));
        }
        public void UpdateThanhvienHD(ThanhvienHDEntines gv)
        {
            _gvda.UpdateThanhvienHD(ThanhvienHD_Parameter(gv));
        }
        public void DeletetThanhvienHD(ThanhvienHDEntines gv)
        {
            SqlParameter[] pr = new SqlParameter[1];
            SqlParameter pr0 = new SqlParameter("@MaThanhvien", SqlDbType.NVarChar, 50);
            pr0.Value = gv.MaThanhvien;
            pr[0] = pr0;
            _gvda.DeleteThanhvienHD(pr);
        }
    }
}
