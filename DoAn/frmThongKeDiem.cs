﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Business;

namespace DoAn
{
    public partial class frmThongKeDiem : Form
    {
        public frmThongKeDiem()
        {
            InitializeComponent();
        }

        private void frmThongKeDiem_Load(object sender, EventArgs e)
        {

        }

        private void btnTK_Click(object sender, EventArgs e)
        {
            try
            {
                SinhvienHDBLL _kqbll = new SinhvienHDBLL();
                _kqbll.SelectDiemcuaSV(dgvDiem, float.Parse(txtDiemtu.Text), float.Parse(txtDendiem.Text));
            }
            catch
            {
                MessageBox.Show("Vui lòng nhập đủ 2 đầu điểm và ký hiệu nhập vào phải là chứ số");
            }
        }
    }
}
