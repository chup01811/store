﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmHDDG : Form
    {
        HDDGBLL _hdbll = new HDDGBLL();
        DotTTBLL _dotbll = new DotTTBLL();
        HDDGEntites _hdent = new HDDGEntites();
        public frmHDDG()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            _hdent.MaHDDG = txtMa.Text;
            _hdbll.DeleteHDDG(_hdent);
            frmHDDG_Load(sender, e);
        }

        private void frmHDDG_Load(object sender, EventArgs e)
        {
            _hdbll.SelectAllHDDG(dgvHD);
            _dotbll.SelectAll(cboHDtheoDot);
            _dotbll.SelectAll(cboMaDotTT);
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            _dotbll.SelectHDtheoDot(dgvHD, cboHDtheoDot.SelectedValue.ToString());
        }

        private void dgvHD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvHD.CurrentRow.Cells[0].Value.ToString();
            txtND.Text = dgvHD.CurrentRow.Cells[1].Value.ToString();
            cboMaDotTT.SelectedValue = dgvHD.CurrentRow.Cells[2].Value.ToString();
            dtpNgayBVDK.Value = DateTime.Parse(dgvHD.CurrentRow.Cells[3].Value.ToString());
            dtpNgayBVCT.Value = DateTime.Parse(dgvHD.CurrentRow.Cells[4].Value.ToString());

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            {
                MessageBox.Show("Mã HD không đhược để trống");
            }
            else
            {


                try
                {
                    _hdent.MaHDDG = txtMa.Text;
                    _hdent.ND = txtND.Text;
                    _hdent.MaDotTT = cboHDtheoDot.SelectedValue.ToString();
                    _hdent.NgayBVdukien = dtpNgayBVDK.Value;
                    _hdent.NgayBVchinhthuc = dtpNgayBVCT.Value;
                    _hdbll.InsertHDDG(_hdent);
                    frmHDDG_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Hội đồng không được trùng lặp vui lòng điền đầy đủ thông tin");
                }

            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _hdent.MaHDDG = txtMa.Text;
            _hdent.ND = txtND.Text;
            _hdent.MaDotTT = cboHDtheoDot.SelectedValue.ToString();
            _hdent.NgayBVdukien = dtpNgayBVDK.Value;
            _hdent.NgayBVchinhthuc = dtpNgayBVCT.Value;
            _hdbll.UpdateHDDG(_hdent);
            frmHDDG_Load(sender, e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {

            txtMa.Clear();
            txtND.Clear();
           
            _hdbll.SelectAllHDDG(dgvHD);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }
    }
}
