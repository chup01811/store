﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmSinhvienHD : Form
    {
        SinhvienHDBLL _svbll = new SinhvienHDBLL();
        SinhvienHDEntines _svent = new SinhvienHDEntines();
        HDDGBLL _hdbll = new HDDGBLL();
        SinhvienBLL _svbll2 = new SinhvienBLL();
        public frmSinhvienHD()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmSinhvienHD_Load(object sender, EventArgs e)
        {
            _svbll.SelectAllSinhvien(dgvSinhvienHD);
            _hdbll.SelectAll(cboMaHD);
            _svbll2.SelectAllMa(cboMaSV);
        }

        private void dgvSinhvienHD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            txtMa.Text = dgvSinhvienHD.CurrentRow.Cells[0].Value.ToString();
            cboMaHD.SelectedValue = dgvSinhvienHD.CurrentRow.Cells[1].Value.ToString();
            cboMaSV.SelectedValue = dgvSinhvienHD.CurrentRow.Cells[2].Value.ToString();
            //txtGhichu.Text = dgvSinhvienHD.CurrentRow.Cells[3].Value.ToString();
            txtDiem.Text = dgvSinhvienHD.CurrentRow.Cells[3].Value.ToString();
            txtTGbv.Text = dgvSinhvienHD.CurrentRow.Cells[4].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            //if (txtMa.Text == "")
            //{ MessageBox.Show("Mã Sinhvien HD không được trống"); }
            //else if (txtDiem.Text == "")
            //{ MessageBox.Show("Điểm không được trống"); }
           // else
            //{
                //try
                //{
                    _svent.Ma = txtMa.Text;
                    _svent.MaHDDG = cboMaHD.SelectedValue.ToString();
                    _svent.MaSV = cboMaSV.SelectedValue.ToString();
                    //_svent.Ghichu = DateTime.Parse(txtGhichu.Text.ToString());
                    _svent.Diem = float.Parse(txtDiem.Text.ToString());
                    _svent.Thoigianbaove = txtTGbv.Text;
                    _svbll.InsertSinhvienHD(_svent);
                    frmSinhvienHD_Load(sender, e);
                //}
                //catch
                //{
                    MessageBox.Show("Mã Sinhvien HD không được trùng lặp vui lòng điền đầy đủ thông tin");
                //}
            //}
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _svent.Ma = txtMa.Text;
            _svent.MaHDDG = cboMaHD.SelectedValue.ToString();
            _svent.MaSV = cboMaSV.SelectedValue.ToString();
           // _svent.Ghichu = DateTime.Parse(txtGhichu.Text.ToString());
            _svent.Diem = float.Parse(txtDiem.Text.ToString());
            _svent.Thoigianbaove = txtTGbv.Text;
            _svbll.UpdateStudentHD(_svent);
            frmSinhvienHD_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _svent.Ma = txtMa.Text;
            _svbll.DeleteStudentHD(_svent);
            frmSinhvienHD_Load(sender, e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtDiem.Clear();
            _svbll.SelectAllSinhvien(dgvSinhvienHD);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
