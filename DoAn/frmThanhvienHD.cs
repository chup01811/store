﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmThanhvienHD : Form 
    {
        ThanhvienHDEntines _gvent = new ThanhvienHDEntines();
        GVHDBLL _gvbll = new GVHDBLL();
        HDDGBLL _hdbll = new HDDGBLL();
        ThanhvienHDBLL _tvhdbll = new ThanhvienHDBLL();
        public frmThanhvienHD()
        {
            InitializeComponent();
        }

        private void frmThanhvienHD_Load(object sender, EventArgs e)
        {
            _tvhdbll.SelectAllThanhvienHD(dgvtvhd);
            _gvbll.SelectAll(cboGV);
            _hdbll.SelectAll(cboHD);
        }

        private void dgvtvhd_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvtvhd.CurrentRow.Cells[0].Value.ToString();
            cboHD.SelectedValue = dgvtvhd.CurrentRow.Cells[1].Value.ToString();
            cboGV.SelectedValue = dgvtvhd.CurrentRow.Cells[2].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMa.Text=="")
                { MessageBox.Show("Ban chua nhap Ma thanh vien"); }
                else { 
                _gvent.MaThanhvien = txtMa.Text;

                _gvent.MaHDDG = cboHD.SelectedValue.ToString();
                _gvent.MaGV = cboGV.SelectedValue.ToString();

                _tvhdbll.InsertThanhvienHD(_gvent);
                frmThanhvienHD_Load(sender, e);
                }
            }
            catch
            {
                MessageBox.Show("Mã Thành viên không được trùng lặp vui lòng điền đầy đủ thông tin");
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _gvent.MaThanhvien = txtMa.Text;

            _gvent.MaHDDG = cboHD.SelectedValue.ToString();
            _gvent.MaGV = cboGV.SelectedValue.ToString();

            _tvhdbll.UpdateThanhvienHD(_gvent);
            frmThanhvienHD_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _gvent.MaThanhvien = txtMa.Text;
            _tvhdbll.DeletetThanhvienHD(_gvent);
            frmThanhvienHD_Load(sender, e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            frmThanhvienHD_Load(sender, e);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }
    }
}
