﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class DiadiemDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet DiadiemSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblDiadiemSelect_All");
            return ds;
        }

        public void InsertDiadiem(SqlParameter[] pr)
        {
            _Data.Insert("tblDiadiemInsert", pr);
        }
        public void UpdateDiadiem
            (SqlParameter[] pr)
        {
            _Data.Update("tblDiadiem_Update", pr);
        }
        public void DeleteDiadiem(SqlParameter[] pr)
        {
            _Data.Update("tblDiadiemDelete", pr);
        }
    }
}
