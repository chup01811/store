﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class DotTTDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet DotTTSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblDotTTSelect_All");
            return ds;
        }
        public DataSet SelectHDtheoDOTbBywhere(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByWhere(storename, pr);
            return ds;
        }

        public DataSet SelectSVtheoDOTbBywhere(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByWhere(storename, pr);
            return ds;
        }
        // public DataSet DetaitheoDotTT(SqlParameter pr)
        //{
        // DataSet ds = new DataSet();
        // ds = _Data.SelelectByName("tblDetaitheoDOT", pr);
        // return ds;
        //}
        public DataSet SelectDetaitheoDotTT(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByWhere(storename, pr);
            return ds;
        }
        public void InsertDotTT(SqlParameter[] pr)
        {
            _Data.Insert("tblDotTT_Insert", pr);
        }
        public void UpdateDotTT(SqlParameter[] pr)
        {
            _Data.Update("tblDotTT_Update", pr);
        }
        public void DeleteDotTT(SqlParameter[] pr)
        {
            _Data.Update("tblDotTT_Delete", pr);
        }
    }
}
