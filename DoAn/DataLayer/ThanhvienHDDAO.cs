﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class ThanhvienHDDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet ThanhvienHDSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblThanhvienHDSelect_All");
            return ds;
        }

        public void InsertThanhvienHD(SqlParameter[] pr)
        {
            _Data.Insert("tblThanhvienHDInsert", pr);
        }
        public void UpdateThanhvienHD
            (SqlParameter[] pr)
        {
            _Data.Update("tblThanhvienHDUpdate", pr);
        }
        public void DeleteThanhvienHD(SqlParameter[] pr)
        {
            _Data.Update("tblThanhvienHDDelete", pr);
        }
    }
}
