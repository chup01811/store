﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class DetaiDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet DetaiSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblDetaiSelect_All");
            return ds;
        }

        public void InsertDetai(SqlParameter[] pr)
        {
            _Data.Insert("tblDetaiInsert", pr);
        }
        public void UpdateDetai
            (SqlParameter[] pr)
        {
            _Data.Update("tblDetaiUpdate", pr);
        }
        public void DeleteDetai(SqlParameter[] pr)
        {
            _Data.Update("tblDetaiDelete", pr);
        }
    }
}
