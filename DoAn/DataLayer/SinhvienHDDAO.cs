﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class SinhvienHDDAO
    {

        ClsDATA _Data = new ClsDATA();

        public DataSet SinhvienHDSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblSinhvienHDSelect_All");
            return ds;
        }
        public DataSet SelectDiemcuaSVe(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByWhere(storename, pr);
            return ds;
        }
        public void InsertSinhvienHD(SqlParameter[] pr)
        {
            _Data.Insert("tblSinhvienHDInsert", pr);
        }
        public void UpdateSinhvienHD
            (SqlParameter[] pr)
        {
            _Data.Update("tblSinhvienHDUpdate", pr);
        }
        public void DeleteSinhvienHD(SqlParameter[] pr)
        {
            _Data.Update("tblSinhvienHDDelete", pr);
        }
    }
}
