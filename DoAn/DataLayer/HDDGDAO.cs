﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class HDDGDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet HDDGSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblHDDGSelect_All");
            return ds;
        }
        public DataSet SelectSVtheoHDbBywhere(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByWhere(storename, pr);
            return ds;
        }
        public void InsertHDDG(SqlParameter[] pr)
        {
            _Data.Insert("tblHDDG_Insert", pr);
        }
        public void UpdateHDDG(SqlParameter[] pr)
        {
            _Data.Update("tblHDDG_Update", pr);
        }
        public void DeleteHDDG(SqlParameter[] pr)
        {
            _Data.Update("tblHDDGDelete", pr);
        }
    }
}
