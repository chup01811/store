﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class ClsConnection
    {
        //code ketnoi voi SQL
        SqlConnection _Conn = new SqlConnection(@"Data Source=DESKTOP-7AV5V36\SQLEXPRESS01;Initial Catalog=SVTT;Integrated Security=True");
        public ClsConnection()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //Mo ket noi voi SQL
        public SqlConnection OpenConnect()
        {
            if (_Conn.State == ConnectionState.Closed)
            {
                _Conn.Open();
            }
            return _Conn;
        }

        //Dóng kết nối
        public SqlConnection CloseConnect()
        {
            if (_Conn.State == ConnectionState.Open)
            {
                _Conn.Close();
            }
            return _Conn;
        }
    }
}
