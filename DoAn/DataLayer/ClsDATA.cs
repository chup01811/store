﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class ClsDATA
    {
        SqlConnection _Con = new SqlConnection();
        ClsConnection _ClsConn = new ClsConnection();

        public DataSet ReadDatastorename(string storename)
        {
            SqlConnection _Conn = new SqlConnection();
            DataSet ds = new DataSet();
            _Conn = _ClsConn.OpenConnect();
            SqlCommand _Com = new SqlCommand();
            _Com.Connection = _Conn;
            _Com.CommandType = CommandType.StoredProcedure;
            _Com.CommandText = storename;
            SqlDataAdapter _ADapt = new SqlDataAdapter(_Com);
            _ADapt.Fill(ds);
            return ds;
        }

        public DataSet ReadDatastorename(string storename, SqlParameter pr)
        {
            SqlConnection _Conn = new SqlConnection();
            DataSet ds = new DataSet();
            _Conn = _ClsConn.OpenConnect();
            SqlCommand _Com = new SqlCommand();
            _Com.Connection = _Conn;
            _Com.CommandType = CommandType.StoredProcedure;
            _Com.CommandText = storename;
            SqlDataAdapter _ADapt = new SqlDataAdapter(_Com);
            _ADapt.Fill(ds);
            return ds;
        }


        public DataSet SelectALL(string storename)
        {
            DataSet ds = new DataSet();
            _Con = _ClsConn.OpenConnect();
            SqlCommand _objCom = new SqlCommand();
            _objCom.Connection = _Con;
            _objCom.CommandType = CommandType.StoredProcedure;
            _objCom.CommandText = storename;
            SqlDataAdapter _objAdap = new SqlDataAdapter(_objCom);
            _objAdap.Fill(ds);
            _Con.Close();
            return ds;
        }



        public DataSet SelelectByID(string storename, SqlParameter pr)
        {
            DataSet ds = new DataSet();
            _Con = _ClsConn.OpenConnect();
            SqlCommand _objCom = new SqlCommand();
            _objCom.Connection = _Con;
            _objCom.CommandType = CommandType.StoredProcedure;
            _objCom.CommandText = storename;
            _objCom.Parameters.Add(pr);
            SqlDataAdapter _objAdap = new SqlDataAdapter(_objCom);
            _objAdap.Fill(ds);
            _Con.Close();
            return ds;
        }


        public DataSet SelelectByName(string storename, SqlParameter pr)
        {
            DataSet ds = new DataSet();
            _Con = _ClsConn.OpenConnect();

            SqlCommand _Comm = new SqlCommand();
            _Comm.Connection = _Con;
            _Comm.CommandType = CommandType.StoredProcedure;
            _Comm.CommandText = storename;
            _Comm.Parameters.Add(pr);
            SqlDataAdapter _Adapt = new SqlDataAdapter(_Comm);
            _Adapt.Fill(ds);

            _Con.Close();
            return ds;
        }





        public void Insert(string storename, SqlParameter[] pr)
        {
            _Con = _ClsConn.OpenConnect();
            SqlCommand _Comm = new SqlCommand();
            _Comm.Connection = _Con;
            _Comm.CommandType = CommandType.StoredProcedure;
            _Comm.CommandText = storename;
            foreach (SqlParameter pr1 in pr)
            {
                _Comm.Parameters.Add(pr1);
            }
            _Comm.ExecuteNonQuery();
            _ClsConn.CloseConnect();
        }



        public void Update(string storename, SqlParameter[] pr)
        {
            _Con = _ClsConn.OpenConnect();
            SqlCommand _objCom = new SqlCommand();
            _objCom.Connection = _Con;
            _objCom.CommandType = CommandType.StoredProcedure;
            _objCom.CommandText = storename;
            foreach (SqlParameter pr1 in pr)
            {
                _objCom.Parameters.Add(pr1);
            }
            _objCom.ExecuteNonQuery();
            _ClsConn.CloseConnect();
        }
        public DataSet SelelectByWhere(string storename, SqlParameter[] pr)
        {
            DataSet ds = new DataSet();
            _Con = _ClsConn.OpenConnect();
            SqlCommand _objCom = new SqlCommand();
            _objCom.Connection = _Con;
            _objCom.CommandType = CommandType.StoredProcedure;
            _objCom.CommandText = storename;
            foreach (SqlParameter pr1 in pr)
            {
                _objCom.Parameters.Add(pr1);
            }
            SqlDataAdapter _objAdap = new SqlDataAdapter(_objCom);
            _objAdap.Fill(ds);
            _Con.Close();
            return ds;
        }
    }
}
