﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class SinhvienTTDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet SinhvienTTSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblSinhvienTTSelect_All");
            return ds;
        }

        public void InsertSinhvienTT(SqlParameter[] pr)
        {
            _Data.Insert("tblSinhvienTTInsert", pr);
        }
        public void UpdateSinhvienTT
            (SqlParameter[] pr)
        {
            _Data.Update("tblSinhvienTTUpdate", pr);
        }
        public void DeleteSinhvienTT(SqlParameter[] pr)
        {
            _Data.Update("tblSinhvienTTDelete", pr);
        }
    }
}
