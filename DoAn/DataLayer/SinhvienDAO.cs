﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class SinhvienDAO
    {
        ClsDATA _Data = new ClsDATA();

        public DataSet SinhvienSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblSinhvienSelect_ALL");
            return ds;
        }

        public DataSet SinhvienSelectByID(SqlParameter pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByID("tblSinhvienSelectBYID", pr);
            return ds;
        }

        public DataSet SinhvienSelectByName(SqlParameter pr)
        {
            DataSet ds = new DataSet();
            ds = _Data.SelelectByName("tblSinhvienSelectBYName", pr);
            return ds;
        }




        public void InsertSinhVien(SqlParameter[] pr)
        {
            _Data.Insert("tblSinhvien_Insert", pr);
        }
        public void UpdateSinhVien(SqlParameter[] pr)
        {
            _Data.Update("tblSinhvien_Update", pr);
        }
        public void DeleteSinhVien(SqlParameter[] pr)
        {
            _Data.Update("tblSinhvienDelete", pr);
        }
    }
}
