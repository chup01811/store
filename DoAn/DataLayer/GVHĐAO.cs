﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DoAn.DataLayer
{
    class GVHĐAO
    {

        ClsDATA _Data = new ClsDATA();

        public DataSet GVHDSelectAll()
        {
            DataSet ds = new DataSet();
            ds = _Data.SelectALL("tblGVHDSelect_All");
            return ds;
        }

        public void InsertGVHD(SqlParameter[] pr)
        {
            _Data.Insert("tblGVHDInsert", pr);
        }
        public void UpdateGVHD(SqlParameter[] pr)
        {
            _Data.Update("tblGVHDUpdate", pr);
        }
        public void DeleteGVHD(SqlParameter[] pr)
        {
            _Data.Update("tblGVHDDelete", pr);
        }
    }
}
