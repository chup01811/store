﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmThongKeDanhsachSVtheDOT : Form
    {
        SinhvienBLL _svbll = new SinhvienBLL();
        DotTTBLL _dotbll = new DotTTBLL();
        public frmThongKeDanhsachSVtheDOT()
        {
            InitializeComponent();
        }

        private void frmThongKeDanhsachSVtheDOT_Load(object sender, EventArgs e)
        {
            _dotbll.SelectAll(cboSVtheoDotTT);
        }

        private void btnTK_Click(object sender, EventArgs e)
        {
            try { _dotbll.SelectSinhvientheoDot(dgvSVtheoDoyTT, cboSVtheoDotTT.SelectedValue.ToString()); }
            catch
            {
                MessageBox.Show("Vui lòng chọn đợt thực tập");
            }
        }

        private void btnBC_Click(object sender, EventArgs e)
        {
            crytalSVtheoDotTT _rpSVtheoDOT = new crytalSVtheoDotTT();
            DataSet _dataset = new DataSet();
            _dataset = _dotbll.SelectSinhvientheoDot(cboSVtheoDotTT.SelectedValue.ToString());
            _rpSVtheoDOT.SetDataSource(_dataset.Tables[0]);

            frmCrytalSVtheoDoiTT _rpdvtheod = new frmCrytalSVtheoDoiTT();
            _rpdvtheod.crystalReportViewer1.ReportSource = _rpSVtheoDOT;
            _rpdvtheod.Show();
        }
    }
}
