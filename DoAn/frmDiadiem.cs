﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmDiadiem : Form
    {
        DiadiemEntites _ddent = new DiadiemEntites();
        DiadiemBLL _ddbll = new DiadiemBLL();
        public frmDiadiem()
        {
            InitializeComponent();
        }

        private void frmDiadiem_Load(object sender, EventArgs e)
        {
            _ddbll.SelectAllDiadiem(dgvDiadiem);
            
        }

        private void dgvDiadiem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvDiadiem.CurrentRow.Cells[0].Value.ToString();
            txtTen.Text = dgvDiadiem.CurrentRow.Cells[1].Value.ToString();
            txtDiachi.Text = dgvDiadiem.CurrentRow.Cells[2].Value.ToString();
            txtSDT.Text = dgvDiadiem.CurrentRow.Cells[3].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            {
                MessageBox.Show("Mã Địa điểm không được để trống");
            }
            else if (txtDiachi.Text == "")
            { MessageBox.Show(" Địa điểm không được trống"); }
            else
            {
                try
                {
                    _ddent.MaDiadiem = txtMa.Text;
                    _ddent.TenDiadiem = txtTen.Text;
                    _ddent.Diachi = txtDiachi.Text;
                    _ddent.SDT = txtSDT.Text;
                    _ddbll.InsertDiadiem(_ddent);
                    frmDiadiem_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Địa điểm không được trùng lặp vui lòng điền đầy đủ thông tin");
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _ddent.MaDiadiem = txtMa.Text;
            _ddent.TenDiadiem = txtTen.Text;
            _ddent.Diachi = txtDiachi.Text;
            _ddent.SDT = txtSDT.Text;
            _ddbll.UpdateDiadiem(_ddent);
            frmDiadiem_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _ddent.MaDiadiem = txtMa.Text;
            _ddbll.DeleteDiadiem(_ddent);
            frmDiadiem_Load(sender, e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtTen.Clear();
            txtDiachi.Clear();
            txtSDT.Clear();
            _ddbll.SelectAllDiadiem(dgvDiadiem);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }
    }
}
