﻿
namespace DoAn
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.quảnLýSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýĐợtThựcTậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýKếtQuảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýHDDGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýĐịaĐiểmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýGVHDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýĐềTàiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêBáoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thosngKêĐiểmCủaSinhViênToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.quảnLýSinhViênThựcTậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thànhViênHộiĐồngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLýSinhViênToolStripMenuItem,
            this.quảnLýĐợtThựcTậpToolStripMenuItem,
            this.quảnLýKếtQuảToolStripMenuItem,
            this.quảnLýHDDGToolStripMenuItem,
            this.quảnLýĐịaĐiểmToolStripMenuItem,
            this.quảnLýGVHDToolStripMenuItem,
            this.quảnLýĐềTàiToolStripMenuItem,
            this.thốngKêBáoCáoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 30);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1288, 30);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // quảnLýSinhViênToolStripMenuItem
            // 
            this.quảnLýSinhViênToolStripMenuItem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.quảnLýSinhViênToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýSinhViênToolStripMenuItem.Name = "quảnLýSinhViênToolStripMenuItem";
            this.quảnLýSinhViênToolStripMenuItem.Size = new System.Drawing.Size(147, 26);
            this.quảnLýSinhViênToolStripMenuItem.Text = "Quản lý Sinh viên";
            this.quảnLýSinhViênToolStripMenuItem.Click += new System.EventHandler(this.quảnLýSinhViênToolStripMenuItem_Click);
            // 
            // quảnLýĐợtThựcTậpToolStripMenuItem
            // 
            this.quảnLýĐợtThựcTậpToolStripMenuItem.BackColor = System.Drawing.Color.Red;
            this.quảnLýĐợtThựcTậpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýĐợtThựcTậpToolStripMenuItem.Name = "quảnLýĐợtThựcTậpToolStripMenuItem";
            this.quảnLýĐợtThựcTậpToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.quảnLýĐợtThựcTậpToolStripMenuItem.Text = "Quản Lý Đợt Thực tập";
            this.quảnLýĐợtThựcTậpToolStripMenuItem.Click += new System.EventHandler(this.quảnLýĐợtThựcTậpToolStripMenuItem_Click);
            // 
            // quảnLýKếtQuảToolStripMenuItem
            // 
            this.quảnLýKếtQuảToolStripMenuItem.BackColor = System.Drawing.Color.Blue;
            this.quảnLýKếtQuảToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýKếtQuảToolStripMenuItem.Name = "quảnLýKếtQuảToolStripMenuItem";
            this.quảnLýKếtQuảToolStripMenuItem.Size = new System.Drawing.Size(137, 26);
            this.quảnLýKếtQuảToolStripMenuItem.Text = "Quản lý Kết quả";
            this.quảnLýKếtQuảToolStripMenuItem.Click += new System.EventHandler(this.quảnLýKếtQuảToolStripMenuItem_Click);
            // 
            // quảnLýHDDGToolStripMenuItem
            // 
            this.quảnLýHDDGToolStripMenuItem.BackColor = System.Drawing.Color.Lime;
            this.quảnLýHDDGToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýHDDGToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.quảnLýHDDGToolStripMenuItem.Name = "quảnLýHDDGToolStripMenuItem";
            this.quảnLýHDDGToolStripMenuItem.Size = new System.Drawing.Size(126, 26);
            this.quảnLýHDDGToolStripMenuItem.Text = "Quản lý HDDG";
            this.quảnLýHDDGToolStripMenuItem.Click += new System.EventHandler(this.quảnLýHDDGToolStripMenuItem_Click);
            // 
            // quảnLýĐịaĐiểmToolStripMenuItem
            // 
            this.quảnLýĐịaĐiểmToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýĐịaĐiểmToolStripMenuItem.Name = "quảnLýĐịaĐiểmToolStripMenuItem";
            this.quảnLýĐịaĐiểmToolStripMenuItem.Size = new System.Drawing.Size(147, 26);
            this.quảnLýĐịaĐiểmToolStripMenuItem.Text = "Quản lý Địa điểm";
            this.quảnLýĐịaĐiểmToolStripMenuItem.Click += new System.EventHandler(this.quảnLýĐịaĐiểmToolStripMenuItem_Click);
            // 
            // quảnLýGVHDToolStripMenuItem
            // 
            this.quảnLýGVHDToolStripMenuItem.BackColor = System.Drawing.Color.Yellow;
            this.quảnLýGVHDToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýGVHDToolStripMenuItem.Name = "quảnLýGVHDToolStripMenuItem";
            this.quảnLýGVHDToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.quảnLýGVHDToolStripMenuItem.Text = "Quản lý GVHD";
            this.quảnLýGVHDToolStripMenuItem.Click += new System.EventHandler(this.quảnLýGVHDToolStripMenuItem_Click);
            // 
            // quảnLýĐềTàiToolStripMenuItem
            // 
            this.quảnLýĐềTàiToolStripMenuItem.BackColor = System.Drawing.Color.Fuchsia;
            this.quảnLýĐềTàiToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýĐềTàiToolStripMenuItem.Name = "quảnLýĐềTàiToolStripMenuItem";
            this.quảnLýĐềTàiToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.quảnLýĐềTàiToolStripMenuItem.Text = "Quản lý Đề tài";
            this.quảnLýĐềTàiToolStripMenuItem.Click += new System.EventHandler(this.quảnLýĐềTàiToolStripMenuItem_Click);
            // 
            // thốngKêBáoCáoToolStripMenuItem
            // 
            this.thốngKêBáoCáoToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.thốngKêBáoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1,
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1,
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem,
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem});
            this.thốngKêBáoCáoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thốngKêBáoCáoToolStripMenuItem.Name = "thốngKêBáoCáoToolStripMenuItem";
            this.thốngKêBáoCáoToolStripMenuItem.Size = new System.Drawing.Size(153, 26);
            this.thốngKêBáoCáoToolStripMenuItem.Text = "Thống kê, Báo cáo";
            // 
            // thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1
            // 
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1.Name = "thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1";
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1.Size = new System.Drawing.Size(359, 26);
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1.Text = "Thống kê Sinh viên theo Đợt Thực tập";
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1.Click += new System.EventHandler(this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1_Click);
            // 
            // thốngKêSinhViênTheoHDDGToolStripMenuItem1
            // 
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1.Name = "thốngKêSinhViênTheoHDDGToolStripMenuItem1";
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1.Size = new System.Drawing.Size(359, 26);
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1.Text = "Thống kê Sinh viên theo HDDG";
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem1.Click += new System.EventHandler(this.thốngKêSinhViênTheoHDDGToolStripMenuItem1_Click);
            // 
            // thốngKêĐiểmCủaSinhViênToolStripMenuItem
            // 
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem.Name = "thốngKêĐiểmCủaSinhViênToolStripMenuItem";
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem.Size = new System.Drawing.Size(359, 26);
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem.Text = "Thống Kê Điểm của Sinh viên";
            this.thốngKêĐiểmCủaSinhViênToolStripMenuItem.Click += new System.EventHandler(this.thốngKêĐiểmCủaSinhViênToolStripMenuItem_Click);
            // 
            // báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem
            // 
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem.Name = "báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem";
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem.Size = new System.Drawing.Size(359, 26);
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem.Text = "Báo cáo địa điểm theo đợt";
            this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem.Click += new System.EventHandler(this.báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem,
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem,
            this.thosngKêĐiểmCủaSinhViênToolStripMenuItem,
            this.báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(325, 100);
            // 
            // thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem
            // 
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem.Name = "thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem";
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem.Size = new System.Drawing.Size(324, 24);
            this.thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem.Text = "Thống kê Sinh viên theo Đợt thực tập";
            // 
            // thốngKêSinhViênTheoHDDGToolStripMenuItem
            // 
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem.Name = "thốngKêSinhViênTheoHDDGToolStripMenuItem";
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem.Size = new System.Drawing.Size(324, 24);
            this.thốngKêSinhViênTheoHDDGToolStripMenuItem.Text = "Thống kê Sinh viên theo HDDG";
            // 
            // thosngKêĐiểmCủaSinhViênToolStripMenuItem
            // 
            this.thosngKêĐiểmCủaSinhViênToolStripMenuItem.Name = "thosngKêĐiểmCủaSinhViênToolStripMenuItem";
            this.thosngKêĐiểmCủaSinhViênToolStripMenuItem.Size = new System.Drawing.Size(324, 24);
            this.thosngKêĐiểmCủaSinhViênToolStripMenuItem.Text = "Thống kê Điểm của Sinh viên";
            // 
            // báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem
            // 
            this.báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem.Name = "báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem";
            this.báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem.Size = new System.Drawing.Size(324, 24);
            this.báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem.Text = "Báo cáo Địa điểm theo Đợt Thực tập";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tKToolStripMenuItem,
            this.bCToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(97, 52);
            // 
            // tKToolStripMenuItem
            // 
            this.tKToolStripMenuItem.BackColor = System.Drawing.Color.Red;
            this.tKToolStripMenuItem.Name = "tKToolStripMenuItem";
            this.tKToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.tKToolStripMenuItem.Text = "TK";
            // 
            // bCToolStripMenuItem
            // 
            this.bCToolStripMenuItem.BackColor = System.Drawing.SystemColors.Highlight;
            this.bCToolStripMenuItem.Name = "bCToolStripMenuItem";
            this.bCToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.bCToolStripMenuItem.Text = "BC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkOrange;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(390, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 38);
            this.label1.TabIndex = 2;
            this.label1.Text = "ĐỒ ÁN HỌC PHẦN 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Coral;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(189, 429);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(734, 38);
            this.label2.TabIndex = 3;
            this.label2.Text = "QUẢN LÝ THÔNG TIN SINH VIÊN THỰC TẬP";
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quảnLýSinhViênThựcTậpToolStripMenuItem,
            this.thànhViênHộiĐồngToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1288, 30);
            this.menuStrip2.TabIndex = 4;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // quảnLýSinhViênThựcTậpToolStripMenuItem
            // 
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.BackColor = System.Drawing.Color.ForestGreen;
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.Name = "quảnLýSinhViênThựcTậpToolStripMenuItem";
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.Text = "Quản lý Sinh viên Thực tập";
            this.quảnLýSinhViênThựcTậpToolStripMenuItem.Click += new System.EventHandler(this.quảnLýSinhViênThựcTậpToolStripMenuItem_Click);
            // 
            // thànhViênHộiĐồngToolStripMenuItem
            // 
            this.thànhViênHộiĐồngToolStripMenuItem.BackColor = System.Drawing.Color.BurlyWood;
            this.thànhViênHộiĐồngToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thànhViênHộiĐồngToolStripMenuItem.Name = "thànhViênHộiĐồngToolStripMenuItem";
            this.thànhViênHộiĐồngToolStripMenuItem.Size = new System.Drawing.Size(172, 26);
            this.thànhViênHộiĐồngToolStripMenuItem.Text = "Thành Viên Hội Đồng";
            this.thànhViênHộiĐồngToolStripMenuItem.Click += new System.EventHandler(this.thànhViênHộiĐồngToolStripMenuItem_Click);
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCoral;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1288, 790);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHome";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem quảnLýSinhViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýĐợtThựcTậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýKếtQuảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýHDDGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýĐịaĐiểmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýGVHDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýĐềTàiToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêSinhViênTheoHDDGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thosngKêĐiểmCủaSinhViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoĐịaĐiểmTheoĐợtThựcTậpToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêBáoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêSinhViênTheoĐợtThựcTậpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem thốngKêSinhViênTheoHDDGToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem thốngKêĐiểmCủaSinhViênToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoĐịaĐiểmTheoĐợtToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem quảnLýSinhViênThựcTậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thànhViênHộiĐồngToolStripMenuItem;
    }
}