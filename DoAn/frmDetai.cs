﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmDetai : Form
    {
        DetaiEntities _ddent = new DetaiEntities();
        DetaiBLL _ddbll = new DetaiBLL();
        GVHDBLL _gvbll = new GVHDBLL();
        GVHDEntites _gvhdent = new GVHDEntites();
        public frmDetai()
        {
            InitializeComponent();
        }

        private void frmDetai_Load(object sender, EventArgs e)
        {
            _ddbll.SelectAllDetai(dgvDetai);
            _gvbll.SelectAll(cboMaGV);
        }

        private void dgvDetai_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvDetai.CurrentRow.Cells[0].Value.ToString();
            txtDetai.Text = dgvDetai.CurrentRow.Cells[1].Value.ToString();
           cboMaGV.SelectedValue = dgvDetai.CurrentRow.Cells[2].Value.ToString();
            txtGhichu.Text = dgvDetai.CurrentRow.Cells[3].Value.ToString();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            { MessageBox.Show("Mã Đề tài không được trống"); }
            else if (txtDetai.Text == "")
            { MessageBox.Show(" Đề tài không được trống"); }
            else
            {
                try
                {
                    _ddent.MaDetai = txtMa.Text;
                    _ddent.TenDetai = txtDetai.Text;
                    _ddent.MaGV = cboMaGV.SelectedValue.ToString();
                    _ddent.Ghichu = txtGhichu.Text;
                    _ddbll.InsertDietai(_ddent);
                    frmDetai_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Đề tài không được trùng lặp vui lòng điền đầy đủ thông tin");
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _ddent.MaDetai = txtMa.Text;
            _ddent.TenDetai = txtDetai.Text;
            _ddent.MaGV = cboMaGV.SelectedValue.ToString();
            _ddent.Ghichu = txtGhichu.Text;
            _ddbll.UpdateDetai(_ddent);
            frmDetai_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
             
             _ddent.MaDetai = txtMa.Text;
            _ddbll.DeleteDetai(_ddent);
            frmDetai_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Đề tài đã được giao cho Sinh Viên thực tập vui lòng truy cập Bảng Sinh viên thực tập để cập nhập lại");
                }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtDetai.Clear();
            txtGhichu.Clear();
            _ddbll.SelectAllDetai(dgvDetai);

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }
    }
}
