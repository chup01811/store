﻿
namespace DoAn
{
    partial class frmBaoCaoDiadiemTTtheoDot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboDDtheoDotTT = new System.Windows.Forms.ComboBox();
            this.btnBC = new System.Windows.Forms.Button();
            this.dgvDDtheoDotTT = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDDtheoDotTT)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(241, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(603, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "BÁO CÁO ĐỊA ĐIỂM THEO ĐỢT THỰC TẬP";
            // 
            // cboDDtheoDotTT
            // 
            this.cboDDtheoDotTT.FormattingEnabled = true;
            this.cboDDtheoDotTT.Location = new System.Drawing.Point(263, 145);
            this.cboDDtheoDotTT.Name = "cboDDtheoDotTT";
            this.cboDDtheoDotTT.Size = new System.Drawing.Size(378, 24);
            this.cboDDtheoDotTT.TabIndex = 1;
            // 
            // btnBC
            // 
            this.btnBC.BackColor = System.Drawing.Color.Red;
            this.btnBC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBC.Location = new System.Drawing.Point(683, 141);
            this.btnBC.Name = "btnBC";
            this.btnBC.Size = new System.Drawing.Size(107, 31);
            this.btnBC.TabIndex = 2;
            this.btnBC.Text = "BÁO CÁO";
            this.btnBC.UseVisualStyleBackColor = false;
            this.btnBC.Click += new System.EventHandler(this.btnBC_Click);
            // 
            // dgvDDtheoDotTT
            // 
            this.dgvDDtheoDotTT.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvDDtheoDotTT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDDtheoDotTT.Location = new System.Drawing.Point(12, 334);
            this.dgvDDtheoDotTT.Name = "dgvDDtheoDotTT";
            this.dgvDDtheoDotTT.RowHeadersWidth = 51;
            this.dgvDDtheoDotTT.RowTemplate.Height = 24;
            this.dgvDDtheoDotTT.Size = new System.Drawing.Size(1089, 413);
            this.dgvDDtheoDotTT.TabIndex = 3;
            // 
            // frmBaoCaoDiadiemTTtheoDot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSalmon;
            this.ClientSize = new System.Drawing.Size(1113, 759);
            this.Controls.Add(this.dgvDDtheoDotTT);
            this.Controls.Add(this.btnBC);
            this.Controls.Add(this.cboDDtheoDotTT);
            this.Controls.Add(this.label1);
            this.Name = "frmBaoCaoDiadiemTTtheoDot";
            this.Text = "frmDiadiemTTtheoDot";
            this.Load += new System.EventHandler(this.frmBaoCaoDiadiemTTtheoDot_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDDtheoDotTT)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDDtheoDotTT;
        private System.Windows.Forms.Button btnBC;
        private System.Windows.Forms.DataGridView dgvDDtheoDotTT;
    }
}