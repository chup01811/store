﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Business;

namespace DoAn
{
    public partial class frmBaoCaoDiadiemTTtheoDot : Form
    {
        DotTTBLL _dotbll = new DotTTBLL();
        DiadiemBLL _ddbll = new DiadiemBLL();
        public frmBaoCaoDiadiemTTtheoDot()
        {
            InitializeComponent();
        }

        private void frmBaoCaoDiadiemTTtheoDot_Load(object sender, EventArgs e)
        {
            _dotbll.SelectAll(cboDDtheoDotTT);
            _ddbll.SelectAllDiadiem(dgvDDtheoDotTT);
        }

        private void btnBC_Click(object sender, EventArgs e)
        {
            crytalDDtheoDot _crpddtd = new crytalDDtheoDot();
            DataSet ds = new DataSet();
            ds = _dotbll.SelectDiadiemtheoDot(cboDDtheoDotTT.SelectedValue.ToString());
            _crpddtd.SetDataSource(ds.Tables[0]);

            frmCrytalDDtheoDotTT _frmddtheod = new frmCrytalDDtheoDotTT();
            _frmddtheod.crystalReportViewer1.ReportSource = _crpddtd;
            _frmddtheod.Show();
        }
    }
}
