﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class DetaiEntities
    {
        string maDetai;
        string tenDetai;
        string maGV;
        string ghichu;
       


        public string MaDetai { get => maDetai; set => maDetai = value; }
        public string TenDetai { get => tenDetai; set => tenDetai = value; }
        public string MaGV { get => maGV; set => maGV = value; }
        public string Ghichu { get => ghichu; set => ghichu = value; }
        
    }
}
