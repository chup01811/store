﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class DiadiemEntites
    {
        string maDDTT;
        string tendiadiem;
        string diachi;
        string sdt;
        public string MaDiadiem { get => maDDTT; set => maDDTT = value; }
        public string TenDiadiem { get => tendiadiem; set => tendiadiem = value; }
        public string Diachi { get => diachi; set => diachi= value; }
        public string SDT { get => sdt; set => sdt = value; }
    }
}
