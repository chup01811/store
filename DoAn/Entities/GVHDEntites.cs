﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class GVHDEntites
    {
        string maGVHD;
        string tenGV;
        bool gioitinh;
        string email;
        string phone;

        public string MaGV { get => maGVHD; set => maGVHD = value; }
        public string TenGV { get => tenGV; set => tenGV = value; }
        public bool Gioitinh { get => gioitinh; set => gioitinh = value; }
       
        public string Email { get => email; set => email = value; }
        public string SDT { get => phone; set => phone = value; }
    }
}
