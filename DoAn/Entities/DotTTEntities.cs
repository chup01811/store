﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class DotTTEntities
    {
        string maDOtTT;
        string nd;
        DateTime ngaybatdau;
        DateTime ngayktdukien;
        string ghichu;
        public string MaDotTT { get => maDOtTT; set => maDOtTT = value; }
        public string ND { get => nd; set => nd = value; }
        public DateTime NgayBD { get => ngaybatdau; set => ngaybatdau = value; }
        public DateTime NgayKT_Dukien { get => ngayktdukien; set => ngayktdukien = value; }
        public string Ghichu { get => ghichu; set => ghichu = value; }
    }
}
