﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class HDDGEntites
    {
        string maHDDGTT;
        string nd;
        string maDotTT;
        DateTime ngaybvdukien ;
        DateTime ngaybvchinhthuc;

        public string MaHDDG { get => maHDDGTT; set => maHDDGTT = value; }
        public string ND { get => nd; set => nd = value; }
        public string MaDotTT { get => maDotTT; set => maDotTT = value; }
        public DateTime NgayBVdukien { get => ngaybvdukien; set => ngaybvdukien = value; }
        public DateTime NgayBVchinhthuc { get => ngaybvchinhthuc; set => ngaybvchinhthuc = value; }


    }
}
