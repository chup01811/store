﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoAn.Entities
{
    class SinhvienEntities
    {
        string maSV;
        string tenSV;
        DateTime ngaysinh;
        bool gioitinh;
       

        public string MaSV { get => maSV; set => maSV = value; }
        public string TenSV { get => tenSV; set => tenSV = value; }
        public DateTime Ngaysinh { get => ngaysinh; set => ngaysinh = value; }
        public bool Gioitinh { get => gioitinh; set => gioitinh = value; }
        
    }
}
