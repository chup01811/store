﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmGVHD : Form
    {
        GVHDEntites _gvent = new GVHDEntites();
        GVHDBLL _gvbll = new GVHDBLL();
        public frmGVHD()
        {
            InitializeComponent();
        }

        private void frmGVHD_Load(object sender, EventArgs e)
        {
            _gvbll.SelectAllGVHD(dgvGV);
        }

       

        

        

        

        

        

        private void dgvGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvGV.CurrentRow.Cells[0].Value.ToString();
            txtTen.Text = dgvGV.CurrentRow.Cells[1].Value.ToString();
            if (bool.Parse(dgvGV.CurrentRow.Cells[2].Value.ToString()) == true)
                rboNam.Checked = true;
            else
                rboNu.Checked = true;
            txtEmail.Text = dgvGV.CurrentRow.Cells[3].Value.ToString();
            txtPhone.Text = dgvGV.CurrentRow.Cells[4].Value.ToString();
            
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            {
                MessageBox.Show("Mã GV không đhược để trống");
            }
            else if (txtTen.Text == "")
            {
                MessageBox.Show("Tên GV không đhược để trống");
            }
            else
            {
                try
                {
                    _gvent.MaGV = txtMa.Text;
                    _gvent.TenGV = txtTen.Text;
                    _gvent.Gioitinh = rboNam.Checked;
                    _gvent.Email = txtEmail.Text;
                    _gvent.SDT = txtPhone.Text;
                    _gvbll.InsertGVHD(_gvent);
                    frmGVHD_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã Giáo viên không được trùng lặp vui lòng điền đầy đủ thông tin");
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _gvent.MaGV = txtMa.Text;
            _gvent.TenGV = txtTen.Text;
            _gvent.Gioitinh = rboNam.Checked;
            _gvent.Email = txtEmail.Text;
            _gvent.SDT = txtPhone.Text;
            _gvbll.UpdateGVHD(_gvent);
            frmGVHD_Load(sender, e);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _gvent.MaGV = txtMa.Text;
            _gvbll.DeleteGVHD(_gvent);
            frmGVHD_Load(sender, e);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtTen.Clear();
            
            txtPhone.Clear();
            txtEmail.Clear();
            _gvbll.SelectAllGVHD(dgvGV);
        }

        private void btnThoát_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }
    }
}
