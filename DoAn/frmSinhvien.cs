﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DoAn.Entities;
using DoAn.Business;

namespace DoAn
{
    public partial class frmSinhvien : Form
    {
        SinhvienBLL _svbll = new SinhvienBLL();
        DotTTBLL _dotbll = new DotTTBLL();
        SinhvienEntities _svent = new SinhvienEntities();
        public frmSinhvien()
        {
            InitializeComponent();
        }

        private void frmSinhvien_Load(object sender, EventArgs e)
        {
            _svbll.SelectAllSinhvien(dgvSinhvien);
            _dotbll.SelectAll(cboSVtheoDotTT);
           //// _dotbll.SelectAll(cboMaDot);
            
        }

        private void dgvSinhvien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtMa.Text = dgvSinhvien.CurrentRow.Cells[0].Value.ToString();
            txtTen.Text = dgvSinhvien.CurrentRow.Cells[1].Value.ToString();
            dtpNgaysinh.Value = DateTime.Parse(dgvSinhvien.CurrentRow.Cells[2].Value.ToString());
            if (bool.Parse(dgvSinhvien.CurrentRow.Cells[3].Value.ToString()) == true)
                rboNam.Checked = true;
            else
                rboNu.Checked = true;
           
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (txtMa.Text == "")
            {
                MessageBox.Show(" Ban chua nhap Mã sinh viên ");
            }
            else if(txtTen.Text=="")
            {
                MessageBox.Show(" Ban chua nhap Tên sinh viên ");
            }
          
            else
            {
                try
                {
                    _svent.MaSV = txtMa.Text;
                    _svent.TenSV = txtTen.Text;
                    _svent.Ngaysinh = dtpNgaysinh.Value;
                    _svent.Gioitinh = rboNam.Checked;
                    _svbll.InsertSinhvien(_svent);
                    frmSinhvien_Load(sender, e);
                }
                catch
                {
                    MessageBox.Show("Mã sinh viên không được trùng lặp vui lòng điền đầy đủ thông tin");
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _svent.MaSV = txtMa.Text;
            //if (txtTenSV.Text == "")
            //{_svent.TenSV = _svent.TenSV; }
            //else
            //{ _svent.TenSV = txtTenSV.Text; }
            _svent.TenSV = txtTen.Text;
            _svent.Ngaysinh = dtpNgaysinh.Value;
            _svent.Gioitinh = rboNam.Checked;
            _svbll.UpdateStudent(_svent);
            frmSinhvien_Load(sender, e);
        }

        private void btnMa_Click(object sender, EventArgs e)
        {
            _svbll.SelectStudentByID(txtMa.Text, dgvSinhvien);
        }

        private void btnTen_Click(object sender, EventArgs e)
        {
            _svbll.SelectStudentByName(txtTen.Text, dgvSinhvien);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            _svent.MaSV = txtMa.Text;
            _svbll.DeleteStudent(_svent);
            frmSinhvien_Load(sender, e);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult hoi;
            hoi = MessageBox.Show("Bạn có muốn thoát không?", "Thoát chương trình", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (hoi == DialogResult.Yes)
                this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtMa.Clear();
            txtTen.Clear();
            _svbll.SelectAllSinhvien(dgvSinhvien);
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            _dotbll.SelectSinhvientheoDot(dgvSinhvien, cboSVtheoDotTT.SelectedValue.ToString());
        }
    }
}
